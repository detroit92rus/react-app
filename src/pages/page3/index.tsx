import { observer } from "mobx-react-lite";
import { FC, useEffect } from "react";
import { Link, useHistory } from "react-router-dom";

import storePage3 from "../../store/page3/index";
import "./style.scss";

export const Page3: FC = observer(() => {
  console.log("3"); //когда произошло изменение на странице
  useEffect(() => {
    console.log("2"); //когда страница создалась
    storePage3.loadPage();
    return () => {
      storePage3.closePage();
      console.log("1"); //когда страничка умерла
    };
  }, []);
  const history = useHistory();
  return (
    <div>
      <Link to="/page0">Домашняя страница</Link>
      {"Count+" + storePage3.count}
      <button onClick={() => storePage3.increment()}>Добавить</button>
      <button onClick={() => storePage3.decrement()}>Убавить</button>
      <div>
        <table>
          <thead>
            <tr>
              <th>id</th>
              <th>Name</th>
              <th>Body</th>
            </tr>
          </thead>
          <tbody>
            {storePage3.post2.map((e) => {
              return (
                <tr
                  className="styleStringTable"
                  onClick={() => {
                    history.push("/page4/" + e.id);
                  }}
                >
                  <td>{e.id}</td>
                  <td>{e.title}</td>
                  <td>{e.body}</td>
                </tr>
              );
            })}
          </tbody>
        </table>
        <button onClick={() => history.goBack()}>Back</button>
      </div>
    </div>
  );
});
