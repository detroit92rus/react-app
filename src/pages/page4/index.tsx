import { observer } from "mobx-react-lite";
import { FC, useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import storePage4 from "../../store/page4/index";
import "./index.scss";

type Tparams = {
  id: string;
  title: string;
  body: string;
};

export const Page4: FC = observer(() => {
  useEffect(() => {
    storePage4.loadPage(params.id);
  });
  const params: Tparams = useParams();
  return (
    <div>
      <div>
        <table>
          <thead>
            <tr>
              <th>id</th>
              <th>Name</th>
              <th>Body</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td className="styleString1">{storePage4.post3.id}</td>
              <td className="styleString2">{storePage4.post3.title}</td>
              <td className="styleString3">{storePage4.post3.body}</td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  );
});
