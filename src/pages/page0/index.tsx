import { FC, useEffect } from "react";
import { Link } from "react-router-dom";
import "./style.scss";
import storePage0 from "../../store/page0/index";
import { observer } from "mobx-react-lite";
const Page0: FC = observer(() => {
  useEffect(() => {
    storePage0.loadPage();
  });
  return (
    <div className="firstBlockWrapper">
      <div className="headerWrapper">
        <span className="headerPosition">Новокузнечный переулок 4/1</span>
        <a className="headerContact" href="tel:+78121234567">
          Позвоните нам!
        </a>
      </div>
      <div id="Main" className="subHeaderWrapper">
        <span className="subHeaderItemCatalog">
          <span className="textStyleUrl">Каталог</span>
        </span>
        <span className="subHeaderItemDelivery">Доставка</span>
        <span className="subHeaderItemCollections">
          <span className="textStyleUrl">Коллекции </span>
        </span>
        <span className="subHeaderItemContacts">
          <Link to="/page3" className="textStyleUrl">
            Контакты
          </Link>
        </span>
      </div>
      {/* {storePage0.post1[0].title} */}
      {storePage0.post1.map((o) => {
        return (
          <div className="wrapperTableData">
            <table className="tableData">
              <td>{o.userId}</td>
              <td>{o.id}</td>
              <td>{o.title}</td>
              <td>{o.body}</td>
            </table>
          </div>
        );
      })}
      <div className="firstBlockButtonsWrapper">
        <Link to="/page2" className="firstBlockButton">
          Винная карта
        </Link>
        <Link to="/page1" className="firstBlockButton">
          Дегустация
        </Link>
      </div>
    </div>
  );
});
export default Page0;
