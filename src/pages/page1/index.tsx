import { FC } from "react";
import { Link } from "react-router-dom";
import "./style.scss";

const Page1: FC = () => {
  return (
    <div className="fourthBlockWrapper">
      <Link to="/page0">Главная страница</Link>
      <div className="fourthBlockContent">
        <div>
          <hr className="border" />
        </div>
        <h1 className="fourthBlockTag">Запись на дегустацию </h1>
        <div>
          <hr className="border" />
        </div>
      </div>
      <div className="fourthBlockBody">
        <span className="fourthBlockBodyText">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Feugiat enim
          tortor in hac id imperdiet adipiscing. Pellentesque nisi, mi sit non
          sit sed fermentum.
        </span>
        <div className="fourthBlockInputsPosition">
          <input className="nameInput" placeholder="Имя" />
          <input className="handyInput" placeholder="Телефон" />
        </div>
        <div className="input">
          <input className="positionInput" placeholder="Бутик на Невском 103" />
        </div>
        <div className="fourthBlockButton">
          <button className="body-button">Записаться</button>
        </div>
      </div>
    </div>
  );
};
export default Page1;
