import { makeAutoObservable } from "mobx";
import axios from "axios";

type Tpost2 = {
  userId: number;
  id: number;
  title: string;
  body: string;
};
class storePage3 {
  getPageId() {
    throw new Error("Method not implemented.");
  }
  count = 0;
  post2: Tpost2[] = [];
  constructor() {
    makeAutoObservable(this);
  }
  closePage = () => {
    this.count = 0;
  };
  loadPage = () => {
    this.count = 0;
    setTimeout(() => {
      this.count = 10;
    }, 5000);
    const apiUrl = "https://jsonplaceholder.typicode.com/posts";
    axios.get(apiUrl).then((resp) => {
      this.post2 = resp.data;
    });
  };
  increment() {
    this.count++;
    console.log(this.count);
  }

  decrement() {
    this.count--;
  }
}
export default new storePage3();
