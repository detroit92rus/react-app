import axios from "axios";
import { makeAutoObservable } from "mobx";
type Tpost3 = {
  id: string;
  userId: string;
  body: string;
  title: string;
};
class storePage4 {
  post3: Tpost3 = { body: "", userId: "", id: "", title: "" };
  constructor() {
    makeAutoObservable(this);
  }
  loadPage = (id: string) => {
    const apiUrl = "https://jsonplaceholder.typicode.com/posts/" + id;
    axios.get(apiUrl).then((resp) => {
      this.post3 = resp.data;
      console.log(this.post3);
    });
  };
}
export default new storePage4();
