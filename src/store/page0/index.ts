import axios from "axios";
import { makeAutoObservable } from "mobx";

type Tpost = {
  userId: number;
  id: number;
  title: string;
  body: string;
};

class storePage0 {
  post1: Tpost[] = [];
  constructor() {
    makeAutoObservable(this);
  }
  loadPage = () => {
    const apiUrl = "https://jsonplaceholder.typicode.com/todos";
    axios.get(apiUrl).then((response) => {
      this.post1 = response.data;
    });
  };
}
export default new storePage0();
