import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Page0 from "./pages/page0/index";
import Page1 from "./pages/page1/index";
import Page2 from "./pages/page2";
import { Page3 } from "./pages/page3";
import { Page4 } from "./pages/page4";

export default function App() {
  return (
    <Router>
      <div>
        <Switch>
          <Route path="/page1">
            <Page1 />
          </Route>
          <Route path="/page2">
            <Page2 />
          </Route>
          <Route path="/page3">
            <Page3 />
          </Route>
          <Route path="/page4/:id">
            <Page4 />
          </Route>
          <Route path="/">
            <Page0 />
          </Route>
        </Switch>
      </div>
    </Router>
  );
}
